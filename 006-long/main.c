#include <stdio.h>
#include <sys/time.h>

int main()
{
  struct timeval t2, t1;
  unsigned long int elapsedTime;
  gettimeofday(&t1, NULL);

  signed long long total = 1;
  for (signed long long i = 1; i < 100000000; ++i)
  {
      total = total + (i * i)/(i+1);
  }

  gettimeofday(&t2, NULL);
  elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000;
  elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000;
  printf("total Execution Time: %lu ms\n", elapsedTime);

  printf("total: %lld\n", total);

  return 0;
}
