#!/bin/bash

for d in */ ; do
    # Check here, there can be an error
    cd "$d" && ([ -f main.cpp ] || [ -f main.c ]) && ./build.sh && cd ..
done
