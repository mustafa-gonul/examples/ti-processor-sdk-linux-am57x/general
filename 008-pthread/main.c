#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>
#include <signal.h>


static sigset_t signalSetUSR1;


int main()
{
    sigset_t signalSetInt;
    sigemptyset(&signalSetInt);
    sigaddset(&signalSetInt, SIGUSR1);


    struct timeval t2, t1;
    unsigned long int elapsedTime;

    gettimeofday(&t1, NULL);

    for(int i = 0;i < 4000;++i)
    {

        pthread_sigmask(SIG_BLOCK, &signalSetInt, NULL);
        pthread_sigmask(SIG_UNBLOCK, &signalSetInt, NULL);

    }

    gettimeofday(&t2, NULL);
    elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000;
    elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000;
    printf("----pthread_sigmask Execution Time: %lu ms\n", elapsedTime);


  return 0;
}
