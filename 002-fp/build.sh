#!/bin/bash

PARAMS=$(diff -y --suppress-common-lines <(arm-linux-gnueabihf-gcc -Q --help=optimizers -O0) <(arm-linux-gnueabihf-gcc -Q --help=optimizers -O1) | awk '{print $1}')

# . ../common/build.sh main.c -Ofast -ffast-math # $PARAMS

# exit

for PARAM in $PARAMS ; do
  echo "-fno-${PARAM:2}"
  . ../common/build.sh main.c -ffast-math -O1 "-fno-${PARAM:2}"
done



