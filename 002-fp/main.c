#include <stdio.h>
#include <math.h>


int main()
{

  for (int i = 0; i < 15; ++i) {
    double a = 9.0;
    double c = pow(a, i);
    long long int l = c;

    /*
    printf("      a = %.15f\n", a);
    printf("      i = %.15f\n", (double) i);
    printf("(d)   c = %.15f\n", c);
    printf("(lli) c    = %.15f\n", (double) l);
    printf("\n");
    */

    if (c - l > 0.001) {
      printf("ERROR!\n");
      return 1;
    }
  }

  printf("SUCCESS!\n");

  return 0;
}
