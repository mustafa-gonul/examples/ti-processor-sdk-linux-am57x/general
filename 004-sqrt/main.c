#include <stdio.h>
#include <math.h>
#include <sys/time.h>

int main()
{
  struct timeval t2, t1;
	unsigned long int elapsedTime;
	gettimeofday(&t1, NULL);

	double total = 0;
	for (double i = 1; i < 10000000; ++i)
	{
		total = total + sqrt(i);
	}

	gettimeofday(&t2, NULL);
	elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000;
	elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000;
	printf("total Execution Time: %lu ms\n", elapsedTime);

	printf("total: %f\n", total);

  return 0;
}

