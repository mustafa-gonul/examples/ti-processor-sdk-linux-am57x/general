#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define VECTOR_SIZE (16)
#define WIDTH       (1280)
#define HEIGHT      (1024)
#define SIZE        (WIDTH * HEIGHT)

int main()
{
  struct timeval t2, t1;
	unsigned long int elapsedTime;
	gettimeofday(&t1, NULL);

  unsigned char *in = (unsigned char *) malloc(SIZE);
  unsigned char *out = (unsigned char *) malloc(SIZE);
  // static unsigned char in[SIZE];
  // static unsigned char out[SIZE];

  for (int i = 1; i < WIDTH - 1; i += VECTOR_SIZE) {				//go sideways by 16 pixel
    for (int j = 1; j < HEIGHT - 1; ++j) { 					//go down by 1 pixel
      out[WIDTH * j] = in[WIDTH * j];
    }
  }

  gettimeofday(&t2, NULL);
	elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000;
	elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000;
	printf("total Execution Time: %lu ms\n", elapsedTime);

  return 0;
}


