#!/bin/bash

# . ../common/build.sh main.c -O3 -ffast-math -fno-code-hoisting -fno-fp-int-builtin-inexact -fno-ipa-bit-cp -fno-ipa-icf-variables -fno-ipa-vrp -fno-loop-interchange -fno-loop-unroll-and-jam -fno-peel-loops -fschedule-fusion -fno-shrink-wrap-separate -fno-split-loops -fno-split-paths -fno-ssa-backprop -fno-store-merging -fno-tree-loop-distribution -fno-tree-vectorize -fno-unsafe-loop-optimizations

. ../common/build.sh main.c -O3 -ffast-math -funsafe-loop-optimizations -fno-align-functions -fno-ipa-cp-alignment -fno-schedule-fusion -fno-strict-overflow -ftree-coalesce-inlined-vars -fno-tree-copyrename -ftree-loop-distribution -ftree-loop-if-convert-stores -ftree-vectorize