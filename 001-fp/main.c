#include <stdio.h>
#include <math.h>


int main()
{

  for (int i = 0; i < 15; ++i) {
    double a = 9.0;
    double c = pow(a, i);

    printf("a = %.15f\n", a);
    printf("b = %.15f\n", (double) i);
    printf("c = %.15f\n", c);
  }

  return 0;
}
