#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define ONE_KB	1024
#define ONE_MB	ONE_KB*1024
#define MEM_SIZE 8*ONE_MB*sizeof(char)


#define FIRST_MALLOC_TEST
#define SECOND_MALLOC_TEST
#define CALLOC_TEST
#define STACK_TEST

int main (void)
{
	char * heap = NULL;
	clock_t start, end;
	double cpu_time_used;

#ifdef FIRST_MALLOC_TEST
	{/*1. Malloc Test Start*/
		{
      printf("--- Malloc Test ---\n");
			start = clock();
			heap = malloc(MEM_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("Allocated with malloc in %fms \n",cpu_time_used);

		}

		{
			start = clock();
			memset(heap,0xCC,MEM_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("1. Set in %fms \n",cpu_time_used);
		}

		{
			start = clock();
			memset(heap,0xAA,MEM_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("2. Set in %fms \n",cpu_time_used);
		}

		{
			start = clock();
			memset(heap,0xCC,MEM_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("3. Set in %fms \n",cpu_time_used);
		}

		{
			FILE *fp;
			start = clock();
			fp = fopen("/dev/null", "a+");
		 	fwrite (heap , sizeof(char), 8*ONE_MB, fp);
			fclose(fp);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("Written to the /dev/null in %fms \n",cpu_time_used);
		}

		{
			start = clock();
			free(heap);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("freed in %fms \n",cpu_time_used);
		}
	}/*1. Malloc Test End*/
#endif

#ifdef CALLOC_TEST
	{/*Calloc test Start*/
		{
      printf("--- Calloc Test ---\n");
			start = clock();
			heap = calloc(1,MEM_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("Allocated with calloc in %fms \n",cpu_time_used);

		}

		{
			start = clock();
			memset(heap,0xCC,MEM_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("1. Set in %fms \n",cpu_time_used);
		}

		{
			start = clock();
			memset(heap,0xAA,MEM_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("2. Set in %fms \n",cpu_time_used);
		}

		{
			start = clock();
			memset(heap,0xCC,MEM_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("3. Set in %fms \n",cpu_time_used);
		}

		{
			FILE *fp;
			start = clock();
			fp = fopen("/dev/null", "a+");
		 	fwrite (heap , sizeof(char), 8*ONE_MB, fp);
			fclose(fp);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("Written to the /dev/null in %fms \n",cpu_time_used);
		}

		{
			start = clock();
			free(heap);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("freed in %fms \n",cpu_time_used);
		}
	}/*Calloc test End*/
#endif

#ifdef SECOND_MALLOC_TEST
	{/*2. Malloc Test Start*/
		{
      printf("--- Malloc Test ---\n");
			start = clock();
			heap = malloc(MEM_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("Allocated with malloc in %fms \n",cpu_time_used);

		}

		{
			start = clock();
			memset(heap,0xCC,MEM_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("1. Set in %fms \n",cpu_time_used);
		}

		{
			start = clock();
			memset(heap,0xAA,MEM_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("2. Set in %fms \n",cpu_time_used);
		}

		{
			start = clock();
			memset(heap,0xCC,MEM_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("3. Set in %fms \n",cpu_time_used);
		}

		{
			FILE *fp;
			start = clock();
			fp = fopen("/dev/null", "a+");
		 	fwrite (heap , sizeof(char), 8*ONE_MB, fp);
			fclose(fp);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("Written to the /dev/null in %fms \n",cpu_time_used);
		}

		{
			start = clock();
			free(heap);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("freed in %fms \n",cpu_time_used);
		}
	}/*2. Malloc Test End*/
#endif
#ifdef STACK_TEST
	{/*Stack Test Start*/
		static char stack[8*ONE_MB];

		{
      printf("--- Stack Test ---\n");
			start = clock();
			memset(stack,0xCC,MEM_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("1. Set from stack in %fms \n",cpu_time_used);
		}

		{
			start = clock();
			memset(stack,0xAA,MEM_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC*1000;
			printf("2. Set from stack in %fms \n",cpu_time_used);
		}
	}/*Stack Test End*/
#endif

	return 0;
}
