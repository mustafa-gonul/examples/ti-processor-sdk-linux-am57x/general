#!/bin/bash

. ../common/build.sh main.c -mcpu=cortex-a15 -mtune=cortex-a15 -mfloat-abi=hard -mfpu=neon-vfpv4 -ffast-math -Ofast
