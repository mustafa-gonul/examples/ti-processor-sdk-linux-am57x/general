#include <stdio.h>
#include <arm_neon.h>
#include <time.h>
#include <sched.h>
#include <sys/time.h>


#define vector_size 16
#define vminmax_u8(a, b) \
    do { \
        uint8x16_t minmax_tmp = (a); \
        (a) = vminq_u8((a), (b)); \
        (b) = vmaxq_u8(minmax_tmp, (b)); \
    } while (0)

void median_3x3_cn(unsigned char *in_data, int cols, unsigned char *out_data)
{
	uint8x16_t q0, q1, q2, q3, q4, q5, q6, q7, q8;
	int i;
	int curpos = 0;
	int start_last_iter = 0;
	unsigned char   * line0, * line1, *line2;
	unsigned char *output;
	int l00_0, l01_0, l02_0, l10_0, l11_0, l12_0, l20_0, l21_0, l22_0;
	int l00_1, l10_1, l10_2, l10_3, l20_1, l20_2;
	int t0_1, t1_1, t1_2;
	int minmax_0, maxmin_0, medmed_0;
	int minmax_1, medmed_1, medmed_2, medmed_3;
	line0 = in_data;
	line1 = line0 + cols;
	line2 = line1 + cols;
	output = out_data;
	l01_0 = l11_0 = l21_0 = 127;
	l02_0 = l12_0 = l22_0 = 127;
	minmax_0 = l02_0;
	maxmin_0 = l22_0;
	for (i = 0; i < 5 ; i++)
		{
			l00_0 = *line0++;
			l10_0 = *line1++;
			l20_0 = *line2++;
			l20_1 = l20_0; l10_1 = l10_0;
			if (l20_0 > l10_0) { l10_1 = l20_0;  l20_1 = l10_0; }
			l00_1 = l00_0; l10_2 = l10_1;
			if (l10_1 > l00_0) { l00_1 = l10_1;  l10_2 = l00_0; }
			l20_2 = l20_1;  l10_3 = l10_2;
			if (l20_1 > l10_2) { l10_3 = l20_1;  l20_2 = l10_2; }
			if (l01_0 < minmax_0) minmax_0 = l01_0;
			if (l00_1 < minmax_0) minmax_0 = l00_1;
			if (l21_0 > maxmin_0) maxmin_0 = l21_0;
			if (l20_2 > maxmin_0) maxmin_0 = l20_2;
			t0_1 = l10_3;
			t1_1 = l11_0;
			if (l10_3 > l11_0)
			{
				t0_1 = l11_0;
				t1_1 = l10_3;
			}
			t1_2 = t1_1;
			if (t1_1 > l12_0)
			{
				t1_2 = l12_0;
			}
			medmed_0  = t1_2;
			if (t0_1 > t1_2) { medmed_0 = t0_1; }
			medmed_1 = medmed_0; minmax_1 = minmax_0;
			if (minmax_0 > medmed_0) { medmed_1 = minmax_0; minmax_1=medmed_0; }
			medmed_2 = medmed_1;
			if (medmed_1 > maxmin_0) { medmed_2 = maxmin_0; }
			medmed_3 = medmed_2;
			if (minmax_1 > medmed_2) { medmed_3 = minmax_1; }
			minmax_0 = l01_0;
			l12_0 = l11_0;
			maxmin_0 = l21_0;
			l01_0 = l00_1;
			l11_0 = l10_3;
			l21_0 = l20_2;
			*output++ = medmed_3;
		}
	curpos = 3; // in case cols < vector_size + 3
	for (i = 3; (i < cols - vector_size) ; i += vector_size)
	{
		q0 = vld1q_u8(&in_data[i + 0]);
		q1 = vld1q_u8(&in_data[i + 1]);
		q2 = vld1q_u8(&in_data[i + 2]);
		q3 = vld1q_u8(&in_data[cols + i + 0]);
		q4 = vld1q_u8(&in_data[cols + i + 1]);
		q5 = vld1q_u8(&in_data[cols + i + 2]);
		q6 = vld1q_u8(&in_data[cols + cols + i + 0]);
		q7 = vld1q_u8(&in_data[cols + cols + i + 1]);
		q8 = vld1q_u8(&in_data[cols + cols + i + 2]);
		vminmax_u8(q0, q3);
		vminmax_u8(q1, q4);
		vminmax_u8(q0, q1);
		vminmax_u8(q2, q5);
		vminmax_u8(q0, q2);
		vminmax_u8(q4, q5);
		vminmax_u8(q1, q2);
		vminmax_u8(q3, q5);
		vminmax_u8(q3, q4);
		vminmax_u8(q1, q3);
		vminmax_u8(q1, q6);
		vminmax_u8(q4, q6);
		vminmax_u8(q2, q6);
		vminmax_u8(q2, q3);
		vminmax_u8(q4, q7);
		vminmax_u8(q2, q4);
		vminmax_u8(q3, q7);
		vminmax_u8(q4, q8);
		vminmax_u8(q3, q8);
		vminmax_u8(q3, q4);
		vst1q_u8(&out_data[i + 2], q4);
		curpos = i;
	}
	start_last_iter = curpos - 2;
	output = out_data + curpos;
	line0 = in_data + start_last_iter;
	line1 = line0 + cols;
	line2 = line1 + cols;
	l01_0 = l11_0 = l21_0 = 127;
	l02_0 = l12_0 = l22_0 = 127;
	minmax_0 = l02_0;
	maxmin_0 = l22_0;
	for (i = start_last_iter; i < cols; i++) {
		l00_0 = *line0++;
		l10_0 = *line1++;
		l20_0 = *line2++;
		l20_1 = l20_0;
		l10_1 = l10_0;
		if (l20_0 > l10_0) {
			l10_1 = l20_0;
			l20_1 = l10_0;
		}
		l00_1 = l00_0;
		l10_2 = l10_1;
		if (l10_1 > l00_0) {
			l00_1 = l10_1;
			l10_2 = l00_0;
		}
		l20_2 = l20_1;
		l10_3 = l10_2;
		if (l20_1 > l10_2) {
			l10_3 = l20_1;
			l20_2 = l10_2;
		}
		if (l01_0 < minmax_0)
			minmax_0 = l01_0;
		if (l00_1 < minmax_0)
			minmax_0 = l00_1;
		if (l21_0 > maxmin_0)
			maxmin_0 = l21_0;
		if (l20_2 > maxmin_0)
			maxmin_0 = l20_2;
		t0_1 = l10_3;
		t1_1 = l11_0;
		if (l10_3 > l11_0) {
			t0_1 = l11_0;
			t1_1 = l10_3;
		}
		t1_2 = t1_1;
		if (t1_1 > l12_0) {
			t1_2 = l12_0;
		}
		medmed_0 = t1_2;
		if (t0_1 > t1_2) {
			medmed_0 = t0_1;
		}
		medmed_1 = medmed_0;
		minmax_1 = minmax_0;
		if (minmax_0 > medmed_0) {
			medmed_1 = minmax_0;
			minmax_1 = medmed_0;
		}
		medmed_2 = medmed_1;
		if (medmed_1 > maxmin_0) {
			medmed_2 = maxmin_0;
		}
		medmed_3 = medmed_2;
		if (minmax_1 > medmed_2) {
			medmed_3 = minmax_1;
		}
		minmax_0 = l01_0;
		l12_0 = l11_0;
		maxmin_0 = l21_0;
		l01_0 = l00_1;
		l11_0 = l10_3;
		l21_0 = l20_2;
		if(i >= curpos){
			*output++ = medmed_3;
		}
	}
}

typedef double        ts_t;
typedef unsigned long ts_clk_t;

static inline ts_t oam_timestamp()
{
  struct timespec ts;
  long long timestamp;
  clock_gettime(CLOCK_REALTIME, &ts); /* or: CLOCK_MONOTONIC */
  timestamp = ts.tv_sec * 1000000000LL + ts.tv_nsec;
  return (double)(timestamp) * 1.0e-6;
}

static inline double oam_elapsed_since(ts_t ts_start)
{
  return (double)(oam_timestamp() - ts_start);
}

#define LOG_CUSTOM(str, ...)  do { printf(str, ##__VA_ARGS__); } while (0)

#define LOG_PROFILE_BEGIN()      \
	ts_t t___ = oam_timestamp(); \
	static ts_t ta___ = 0.0;     \
	static unsigned tc___ = 0;   \
	LOG_CUSTOM(">>> Func: %s Count: %d Total: %f\n", __func__, ++tc___, ta___)

#define LOG_PROFILE_END()                     \
	do {                                      \
		ts_t te___ = oam_elapsed_since(t___); \
		LOG_CUSTOM("<<< Func: %s Time: %f Total: %f\n", __func__, te___, ta___ += te___); \
	} while (0)


#define LOG_PROFILE_CHECKPOINT()              \
	do {                                      \
		ts_t te___ = oam_elapsed_since(t___); \
		LOG_CUSTOM("--- Func: %s Line: %d Time %f\n", __func__, __LINE__, te___); \
	} while (0)

#define LOG_PROFILE_FLUSH() fflush(stdout)

#define LOG_PROFILE_EXIT() exit(0);

#define WIDTH  1280
#define HEIGHT 1024

int main()
{
  unsigned char in[HEIGHT][WIDTH];
  unsigned char out[HEIGHT][WIDTH];

  LOG_PROFILE_BEGIN();

  for(int line = 0; line < HEIGHT - 2; ++line) {
    median_3x3_cn(in[line], WIDTH, out[line]);
	}

  LOG_PROFILE_END();

  return 0;
}
