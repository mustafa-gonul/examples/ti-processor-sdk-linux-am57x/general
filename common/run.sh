#!/bin/bash

NAME=$(basename $(pwd))

function cleanup() {
  pkill "$NAME"
}

trap cleanup EXIT

./"$NAME"

# strace -c ./"$NAME"

