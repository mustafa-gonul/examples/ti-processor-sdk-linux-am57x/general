#!/bin/bash

# Configuration
. ../config.sh

# Variables
# Do not edit if you dont know what you are doing!
NAME=$(basename $(pwd))
DIR="/home/root/$NAME"
BUILD="build"
EXE="$BUILD/$NAME"


# Preparation for the build
[ ! -d "$BUILD" ] && mkdir "$BUILD"
[ -f "$EXE" ] && rm "$EXE"

# Build params
FILE="$1"
COMPILE_PARAMS="${@:2}"
# LINK_PARAMS="-lm -lrt -pthread -lboost_thread -lboost_system"
LINK_PARAMS="-lm -lrt -pthread"
COMPILER="arm-linux-gnueabihf-gcc"

if [ "${FILE#*.}" == "cpp" ]; then
  COMPILER="arm-linux-gnueabihf-g++"
fi

# Building
echo ""
echo "======================================================================================================"
echo "Building $NAME ($FILE) ..."
echo "======================================================================================================"

$COMPILER $COMPILE_PARAMS -g -o $EXE $FILE $LINK_PARAMS


echo ""
echo "======================================================================================================"
echo "Preparing $NAME ($FILE) on the target ..."
echo "======================================================================================================"

# Preparation for copying
ssh $SSH_PARAMS "$ENDPOINT" "[ -d $DIR ] && rm -Rf $DIR"
ssh $SSH_PARAMS "$ENDPOINT" "mkdir -p $DIR"

# Copying
scp $SSH_PARAMS "$EXE"             "$ENDPOINT":"$DIR"
scp $SSH_PARAMS ../common/run.sh   "$ENDPOINT":"$DIR"/run.sh



# Running
echo ""
echo "======================================================================================================"
echo "Running $NAME ($FILE) ..."
echo "======================================================================================================"
echo ""

ssh $SSH_PARAMS "$ENDPOINT" "cd $DIR && ./run.sh"

echo ""
