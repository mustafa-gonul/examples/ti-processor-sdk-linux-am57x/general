#include <boost/lockfree/queue.hpp>
#include <thread>
#include <iostream>

using namespace std;

boost::lockfree::queue<int> queue{128};

int sum_1 = 0;
int sum_2 = 0;


void producer_1(void)
{
}

void producer_2(void)
{
}

void consumer_1(void)
{
}

void consumer_2(void)
{
}


int main(int argc, char* argv[])
{
  thread p_1{producer_1};
  thread p_2{producer_2};
  thread c_1{consumer_1};
  thread c_2{consumer_2};

  p_1.join();
  p_2.join();
  c_1.join();
  c_2.join();


  return 0;
}