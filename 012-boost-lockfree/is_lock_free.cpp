#include <boost/lockfree/queue.hpp>
#include <iostream>

using namespace std;

namespace blf = boost::lockfree;


typedef struct _data {
  uint32_t d0;
  uint32_t d1;
  uint32_t d2;
  uint32_t d3;
  uint32_t d4;
  uint32_t d5;
  uint32_t d6;
  uint32_t d7;
  uint32_t d8;
} uints;


blf::queue<int,   blf::fixed_sized<true>, blf::capacity<0>> iq;
blf::queue<uints, blf::fixed_sized<true>, blf::capacity<3>> uq;




int main(int argc, char* argv[])
{
  cout << "====================================================================================" << endl;
  cout << "blf::queue<int,   blf::fixed_sized<true>, blf::capacity<16>>" << endl;
  if (!iq.is_lock_free())
    cout << "not ";
  cout << "lockfree" << endl;
  cout << "Size: " << sizeof(iq) << endl;
  cout << "int:  " << sizeof(int) << endl;
  cout << "====================================================================================" << endl;

  cout << "====================================================================================" << endl;
  cout << "blf::queue<uints, blf::fixed_sized<true>, blf::capacity<16>>" << endl;
  if (!uq.is_lock_free())
    cout << "not ";
  cout << "lockfree" << endl;
  cout << "Size:  " << sizeof(uq) << endl;
  cout << "units: " << sizeof(uints) << endl;
  cout << "====================================================================================" << endl;

  return 0;
}